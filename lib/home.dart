import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:sociopartnership_flutter/input_panen.dart';
import 'package:sociopartnership_flutter/input_tanam.dart';
import 'package:sociopartnership_flutter/kontrak.dart';
import 'package:sociopartnership_flutter/mitra_tani.dart';
import 'package:sociopartnership_flutter/jual_kering.dart';
import 'package:sociopartnership_flutter/jual_basah.dart';
import 'package:sociopartnership_flutter/constants.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  //BOTTOM NAVIGATION
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];


  // CARD CARAUSELCONTROLLER
  int _current = 0;
  final CarouselController _controller = CarouselController();
  final List<Widget> myData = [
    Container(
      height: 199,
      width: 344,
      color: Colors.red,
      child: Center(child: Text('Picture 1')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.yellow,
      child: Center(child: Text('Picture 2')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.green,
      child: Center(child: Text('Picture 3')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.blue,
      child: Center(child: Text('Picture 4')
      ),
    ),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 8),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            //COSTUM APPBAR
            Container(
              margin: EdgeInsets.only(left: 8, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      image: DecorationImage(
                        image: AssetImage('images/icon_drawer.png')
                      )
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        image: DecorationImage(
                            image: AssetImage('images/icon_man.png')
                        )
                    ),
                  )
                ],
              ),
            ),

            //CARD SECTION
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: EdgeInsets.only(left: 16,bottom: 5, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Hallo, Sulistiyo Aji', style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),),
                  Container(height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        image: DecorationImage(
                            image: AssetImage('images/icon_notifikasi.png')
                        )
                    ),)
                ],
              ),
            ),

            Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CarouselSlider(
                      items: myData,
                      carouselController: _controller,
                      options: CarouselOptions(
                          autoPlay: true,
                          enlargeCenterPage: true,
                          aspectRatio: 3.0,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: myData.asMap().entries.map((entry) {
                        return GestureDetector(
                          onTap: () => _controller.animateToPage(entry.key),
                          child: Container(
                            width: 12.0,
                            height: 12.0,
                            margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: (Theme.of(context).brightness == Brightness.dark
                                    ? Colors.white
                                    : Colors.black)
                                    .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                          ),
                        );
                      }).toList(),
                    ),
                  ]),
            ),

            Padding(padding: EdgeInsets.only(left: 16, bottom: 0, top: 0),
              child: Row(
                children: <Widget>[
                  Text('Pesanggem', style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                  ),),
                ],
              ),
            ),

            Container(
              height: 190,
              child: GridView.count(
                padding: const EdgeInsets.all(6),
                crossAxisCount: 4,
                children: <Widget>[
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const InputTanam()));
                      },
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.add_task, size: 40, color: Colors.red,),
                            Text("INPUT TANAM", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const InputPanen()));
                      },
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.add_box, size: 40, color: Colors.orange,),
                            Text("INPUT PANEN", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const MitraTani()));
                      },
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.join_inner, size: 40, color: Colors.green,),
                            Text("MITRA TANI", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const JualBasah()));},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.sell, size: 40, color: Colors.blue,),
                            Text("JUAL BASAH", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const JualKering()));},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.select_all_sharp, size: 40, color: Colors.brown,),
                            Text("JUAL KERING", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.contact_support_rounded, size: 40, color: Colors.purple,),
                            Text("HALO TANI", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Kontrak()));},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: const <Widget>[
                            Icon(Icons.agriculture, size: 40, color: Colors.grey,),
                            Text("KONTRAK", style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Padding(padding: EdgeInsets.only(left: 16, bottom: 10, top: 0),
              child: Row(
                children: <Widget>[
                  Text('Katalog', style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                  ),),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10.5, right: 10.5),
              height: 200,
              child: ListView(
                children: <Widget>[
                  Card(
                    elevation: 15,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(16),
                            child: Icon(Icons.shop, size: 40, color: Colors.redAccent,),
                          ),
                          Text("Info Toko Saprotan", style: TextStyle(fontWeight: FontWeight.w500),)
                        ],
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(16),
                            child: Icon(Icons.price_check, size: 40, color: Colors.orangeAccent,),
                          ),
                          Text("Info Harga Komoditi", style: TextStyle(fontWeight: FontWeight.w500),)
                        ],
                      ),
                    ),
                  ),
                  Card(
                    elevation: 15,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(16),
                            child: Icon(Icons.dry_sharp, size: 40, color: Colors.greenAccent,),
                          ),
                          Text("Info Pengeringan Mitra", style: TextStyle(fontWeight: FontWeight.w500),),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ),
           ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.production_quantity_limits_outlined),
            label: 'Produk',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.grass),
            label: 'Budidaya',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            label: 'Akun',
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white70,
        unselectedItemColor: Colors.grey,
        selectedItemColor: ColorPalette.primaryColor,
      ),
    );
  }
}
