import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/splashscreen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Splash Screen',
    home: SplashScreen(),
  ));
}