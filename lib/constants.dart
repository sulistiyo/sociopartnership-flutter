import 'package:flutter/material.dart';

class ColorPalette {
  static const primaryColor = Color(0xff2e7d32);
  static const primaryDarkColor = Color(0xffb2ff59);
  static const underlineTextField = Color(0xffa5d6a7);
  static const hintColor = Color(0xffe8f5e9);
}