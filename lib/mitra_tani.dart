import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';

class MitraTani extends StatefulWidget {
  const MitraTani({Key? key}) : super(key: key);

  @override
  State<MitraTani> createState() => _MitraTaniState();
}

class _MitraTaniState extends State<MitraTani> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        title: Text(
            'Silakan Daftar'
        ),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text('Ini adalah halaman registrasi')
          ],
        ),
      ),
    );
  }
}
