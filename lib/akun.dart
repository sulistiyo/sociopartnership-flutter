import 'package:flutter/material.dart';

class Akun extends StatelessWidget {
  const Akun({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'AKUN KU', style: TextStyle(fontSize: 15,color: Colors.red),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/img_cepmek.jpg'),
          fit: BoxFit.cover,
        ),
        border: Border.all(
          color: Colors.green,
          width: 5,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      height: 300,
      width: 300,
    );
  }
}
