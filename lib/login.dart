import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';
import 'package:sociopartnership_flutter/home.dart';
import 'package:sociopartnership_flutter/register.dart';
import 'package:sociopartnership_flutter/hapus.dart';

class Login extends StatelessWidget {

@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Container (
      color: ColorPalette.primaryColor,
      padding: EdgeInsets.all(20),
      child: ListView(
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                _iconLogin(),
                _titleDescription(),
                _textField(),
                _buildButton(context),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
}

Widget _iconLogin() {
  return Image.asset(
    "images/icon_login.png",
    width: 150,
    height: 150,
  );
}

Widget _titleDescription(){
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 16),
      ),
      Text(
        "Login Into App",
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 12),
      ),
      Text(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit,Nullam tincidunt ante lacus",
        style: TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget _textField(){
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 12),
      ),
      TextFormField(
        decoration: const InputDecoration(
          border: UnderlineInputBorder(),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underlineTextField,
              width: 1.5,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white,
              width: 3,
            ),
          ),
          hintText: "Username",
          hintStyle: TextStyle(color: ColorPalette.hintColor),
        ),
        style: TextStyle(color: Colors.white),
        autofocus: false,
      ),
      Padding(
        padding: EdgeInsets.only(top: 12),
      ),
      TextFormField(
        decoration: const InputDecoration(
          border: UnderlineInputBorder(),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underlineTextField,
              width: 1.5,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white,
              width: 3,
            ),
          ),
          hintText: "Password",
          hintStyle: TextStyle(color: ColorPalette.hintColor),
        ),
        style: TextStyle(color: Colors.white),
        obscureText: true,
        autofocus: false,
      ),
    ],
  );
}

Widget _buildButton(BuildContext context) {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 16),
      ),
      InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8),
          width: double.infinity,
          child: Text(
            'Login',
            style: TextStyle(color: ColorPalette.primaryColor),
            textAlign: TextAlign.center,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 16),
      ),
      Text(
        'or',
        style: TextStyle(
          color: Colors.white,
          fontSize: 12,
        ),
      ),
      TextButton (
        child: const Text(
          'Register',
          style: TextStyle(color: Colors.white),
        ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
        },
      ),
    ],
  );
}