import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';

class MyHeaderDrawer extends StatefulWidget {
  const MyHeaderDrawer({super.key});

  @override
  _MyHeaderDrawerState createState() => _MyHeaderDrawerState();
}

class _MyHeaderDrawerState extends State<MyHeaderDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPalette.primaryColor,
      width: double.infinity,
      height: 200,
      padding: const EdgeInsets.only(top: 20),
      child: Column (
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 70,
            decoration: const BoxDecoration(
              shape: BoxShape.rectangle,
              image: DecorationImage(
                  image: AssetImage('images/img_handshake.png'),
              ),
            ),
          ),
          const Text("SocioPartnerShip", style: TextStyle(color: Colors.white, fontSize: 20),),
          const Text("Jagung", style: TextStyle(color: Colors.white, fontSize: 14),),
        ],
      ),
    );
  }
}