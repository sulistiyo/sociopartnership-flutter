import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';
import 'package:sociopartnership_flutter/home.dart';
import 'package:sociopartnership_flutter/header_drawer.dart';
import 'package:sociopartnership_flutter/input_tanam.dart';
import 'package:sociopartnership_flutter/jual_basah.dart';
import 'package:sociopartnership_flutter/jual_kering.dart';
import 'package:sociopartnership_flutter/profil.dart';
import 'package:sociopartnership_flutter/input_panen.dart';
import 'package:sociopartnership_flutter/mitra_tani.dart';
import 'package:sociopartnership_flutter/home.dart';
import 'package:sociopartnership_flutter/jual_kering.dart';

class Hapus extends StatelessWidget {

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];


  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text(
            'MiTANI.Apps',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.5,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: ColorPalette.primaryColor,
      ),
      drawer: Drawer(
        child: SingleChildScrollView(
          child: Column(
            children: [
             const MyHeaderDrawer(),
              ListTile(
                leading: const Icon(Icons.home),
                title: const Text ("Beranda"),
                onTap: () {},
              ),
              ListTile(
                leading: const Icon(Icons.settings),
                title: const Text ("Pengaturan"),
                onTap: () {},
              ),
              ListTile(
                leading: const Icon(Icons.money),
                title: const Text ("Berlangganan"),
                onTap: () {},
              ),
              ListTile(
                leading: const Icon(Icons.people_alt_sharp),
                title: const Text ("Profil"),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const Profil()));
                },
              ),
              ListTile(
                leading: const Icon(Icons.logout_sharp),
                title: const Text ("Keluar"),
                onTap: () {Navigator.pop(context);},
              ),
            ],
          ),
        ),
      ),
      body: Container(
        child: GridView.count(
          padding: const EdgeInsets.all(15),
          crossAxisCount: 3,
          children: <Widget>[
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const InputTanam()));
                },
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.add_task, size: 50, color: Colors.red,),
                      Text("INPUT TANAM", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const InputPanen()));
                },
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.add_box, size: 50, color: Colors.orange,),
                      Text("INPUT PANEN", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const MitraTani()));
                },
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.join_inner, size: 50, color: Colors.green,),
                      Text("MITRA TANI", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const JualBasah()));},
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.sell, size: 50, color: Colors.blue,),
                      Text("JUAL BASAH", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const JualKering()));},
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.select_all_sharp, size: 50, color: Colors.black,),
                      Text("JUAL KERING", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 15,
              margin: const EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: InkWell(
                onTap: (){},
                splashColor: Colors.orange,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.contact_support_rounded, size: 50, color: Colors.purple,),
                      Text("HALO TANI", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.production_quantity_limits_outlined),
            label: 'Produk',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.grass),
            label: 'Budidaya',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            label: 'Akun',
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white70,
        unselectedItemColor: Colors.grey,
        selectedItemColor: ColorPalette.primaryColor,
      ),
    );
  }
}

