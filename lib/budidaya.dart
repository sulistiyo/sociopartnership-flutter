import 'package:flutter/material.dart';

class Budidaya extends StatelessWidget {
  const Budidaya({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'TANAMAN BUDIDAYA', style: TextStyle(fontSize: 15,color: Colors.red),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/img_tanaman.png'),
          fit: BoxFit.cover,
        ),
        border: Border.all(
          color: Colors.green,
          width: 5,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      height: 300,
      width: 300,
    );
  }
}
