import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/budidaya.dart';
import 'package:sociopartnership_flutter/akun.dart';
import 'package:sociopartnership_flutter/constants.dart';

class InputPanen extends StatefulWidget {
  const InputPanen({Key? key}) : super(key: key);

  @override
  _InputPanenState createState() => _InputPanenState();
}

class _InputPanenState extends State<InputPanen> {
 int _selectedTabIndex = 0;

 void _onNavBarTapped(int index){
   setState(() {
     _selectedTabIndex = index;
   });
 }

  @override
  Widget build (BuildContext context){
   final _listPage = <Widget> [
     const Text('Halaman Beranda'),
     const Text('Halaman Produk'),
     //const Text('Budidaya'),
     Budidaya(),
     //const Text('Akun')
     Akun(),
   ];

   final _bottomNavBarItems = <BottomNavigationBarItem>[
     const BottomNavigationBarItem(
       icon: Icon(Icons.home),
       label: 'Beranda',
     ),

     const BottomNavigationBarItem(
       icon: Icon(Icons.production_quantity_limits_outlined),
       label: 'Produk',
     ),

     const BottomNavigationBarItem(
       icon: Icon(Icons.grass),
       label: 'Budidaya',
     ),

     const BottomNavigationBarItem(
       icon: Icon(Icons.account_balance),
       label: 'Akun',
     ),
   ];

   final _bottomNavBar = BottomNavigationBar(
     type: BottomNavigationBarType.fixed,
     backgroundColor: ColorPalette.primaryColor,
     items: _bottomNavBarItems,
     currentIndex: _selectedTabIndex,
     unselectedItemColor: Colors.white54,
     selectedItemColor: Colors.white,
     onTap: _onNavBarTapped,
   );

   return Scaffold(
     appBar: AppBar(
       title: const Text(
           'Input Jumlah Panen'
       ),
       backgroundColor: ColorPalette.primaryColor,
     ),
     body: Center(
       child: _listPage[_selectedTabIndex],
     ),
     bottomNavigationBar: _bottomNavBar,
   );
 }
}
