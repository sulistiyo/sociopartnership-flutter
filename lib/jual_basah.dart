import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';

class JualBasah extends StatelessWidget {
  const JualBasah({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        title: Text(
            'Silakan Daftar'
        ),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: ListView(
        children: <Widget>[
          Card(
            elevation: 15,
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(16),
                  child: Icon(Icons.speaker),
                ),
                Text("Speaker")
              ],
            ),
          ),
          Card(
            elevation: 15,
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(16),
                  child: Icon(Icons.speaker),
                ),
                Text("Speaker")
              ],
            ),
          ),
        ],
      )
    );
  }
}