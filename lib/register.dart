import 'package:flutter/material.dart';
import 'package:sociopartnership_flutter/constants.dart';
import 'package:sociopartnership_flutter/login.dart';

class Register extends StatelessWidget {

  static const routeName = "/register";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: ColorPalette.primaryColor,
          padding: EdgeInsets.all(20),
          child: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  children: <Widget>[
                    _iconRegister(),
                    _titleDescription(),
                    _textField(),
                    _buildButton(context),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }

  Widget _iconRegister(){
    return Image.asset(
      "images/icon_register.png",
      width: 150,
      height: 150,
    );
  }

  Widget _titleDescription(){
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 16),
        ),
        Text(
          "Registration",
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12),
        ),
        Text(
          "jsihfdcsdjlfkhanfsdai gdndcfugn uifhncwuegn",
          style: TextStyle(
            fontSize: 12,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget _textField(){
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12),
        ),
        TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3,
              ),
            ),
            hintText: "Username",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          autofocus: false,
        ),
        Padding(padding: EdgeInsets.only(top: 12),
        ),
        TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3,
              ),
            ),
            hintText: "Password",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          obscureText: true,
          autofocus: false,
        ),
        Padding(
          padding: EdgeInsets.only(top: 12),
        ),
        TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3,
              ),
            ),
            hintText: "Confirm Password",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          obscureText: true,
          autofocus: false,
        ),
      ],
    );
  }

  Widget _buildButton(BuildContext context){
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            width: double.infinity,
            child: Text(
              'Register',
              style: TextStyle(color: ColorPalette.primaryColor),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        Text(
          'or',
          style: TextStyle(
            color: Colors.white,
            fontSize: 12,
          ),
        ),
        TextButton(
          child: Text(
            'Login',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
          },
        ),
      ],
    );
  }
}
