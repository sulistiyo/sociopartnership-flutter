import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:sociopartnership_flutter/constants.dart';

class InputTanam extends StatefulWidget {
  const InputTanam({Key? key}) : super(key: key);

  @override
  State<InputTanam> createState() => _InputTanamState();
}

class _InputTanamState extends State<InputTanam> {

  List<String> data = ["Jagung", "Padi", 'Tebu', "Kambing (Disabled)"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        title: Text(
            'Input Tanggal Tanam'
        ),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              child: Center(
                  child: Padding(
                      padding: EdgeInsets.all(16),
                      child: DropdownSearch<String>(
                        popupProps: PopupProps.menu(
                          showSelectedItems: true,
                          disabledItemFn: (String s) => s.startsWith('K'),
                        ),
                        items: data,
                        dropdownDecoratorProps: DropDownDecoratorProps(
                          dropdownSearchDecoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 20,
                              vertical: 10,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            labelText: "Komoditi",
                            hintText: "pilih komoditi",
                          ),
                        ),
                        onChanged: print,
                        //selectedItem: "Jagung",
                      ),
                  ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 0),
              child: Center(
                  child: Padding(
                      padding: EdgeInsets.all(16),
                      child: DropdownSearch<String>(
                        popupProps: PopupProps.menu(
                          showSelectedItems: true,
                          disabledItemFn: (String s) => s.startsWith('K'),
                        ),
                        items: data,
                        dropdownDecoratorProps: DropDownDecoratorProps(
                          dropdownSearchDecoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 20,
                              vertical: 10,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            labelText: "Komoditi",
                            hintText: "pilih komoditi",
                          ),
                        ),
                        onChanged: print,
                        //selectedItem: "Jagung",
                      )
                  )
              ),
            )
          ],
        ),
      )







    );
  }
}
